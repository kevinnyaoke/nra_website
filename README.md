# nra_website

## Project setup
```
npm install
```

### Compiles and minifies for production
```
npm run build && npm run prod
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
