!(function (e) {

    var topNavHeight = e('#top-nav').height();
    var headerHeight = e('#header').height();
    var dynamicHeight = e('#dynamic').height();
    var dynamicScrollPos = parseInt(topNavHeight+headerHeight);

    e(window).scroll(function () {

        e(this).scrollTop() > dynamicScrollPos ? 
            (  
                e('#dynamic').addClass('fixed inset-x-0 top-0 z-10'), 
                e('body').css('padding-top',dynamicHeight),
                e('#dynamiclogo').removeClass('hidden') ,
                e('#dynamicauth').removeClass('hidden')
            ) 
            : 
            ( 
                e('#dynamic').removeClass('fixed inset-x-0 top-0 z-10'), 
                e('body').css('padding-top',0),
                e('#dynamiclogo').addClass('hidden border-r border-gray-200 py-2 px-4'),
                e('#dynamicauth').addClass('hidden flex')
            );

    });

})(jQuery)